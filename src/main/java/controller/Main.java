package controller;

import exceptions.ExceptionDBAccess;
import exceptions.ExceptionRequest;
import model.beans.Categorie;
import model.beans.Chambre;
import model.beans.Hotel;
import model.beans.TypeHotel;
import model.dao.DAOCategorie;
import model.dao.DAOChambre;
import model.dao.DAOHotel;
import model.dao.DAOTypeHotel;

public class Main {

	public static void main(String[] args) {
		
		//DAOFactory daofactory = new DAOFactory();
		DAOTypeHotel daotypehotel = new DAOTypeHotel();
		DAOHotel daohotel = new DAOHotel();
		DAOCategorie daocategorie = new DAOCategorie();
		DAOChambre daochambre = new DAOChambre();
		
		System.out.println("----------------DAOTYPEHOTEL----------------");
		
		// DAOTYPEHOTEL
		
		TypeHotel t = new TypeHotel(10, "10 �toiles");
		
		// Ajout d'un type
		try {
			daotypehotel.insert(t);
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		t.setNomType("12 �toiles");
		
		// Modification d'un type
		try {
			daotypehotel.update(t);
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		// Suppression d'un type
		try {
			daotypehotel.delete(10);
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		// Recherche d'un type
		try {
			System.out.println(daotypehotel.findById(11).get().getId());
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		// Recherche de tous les types
		try {
			System.out.println(daotypehotel.findAll().getSize());
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		System.out.println("");
		
		System.out.println("----------------DAOHOTEL----------------");
		
		// DAOHOTEL
	
		TypeHotel th;
		try {
			th = daotypehotel.findById(11).get();
			Hotel h = new Hotel(11, th, "Hotel 2 cr��", "21 rue de Bitche", "81000", "Albiche");
			
			// Ajout d'un hotel
			daohotel.insert(h);
			
			// Modification d'un h�tel
			h.setAdresseHotel("2 rue de Java");
			daohotel.update(h);
			
			// Suppression d'un h�tel
			daohotel.delete(11);
			
			// Recherche d'un h�tel
			System.out.println(daohotel.findById(10).get().getId());
			
			
			// Recherche de tous les h�tels
			System.out.println(daohotel.findAll().getSize());
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		System.out.println("");
		
		System.out.println("----------------DAOCATEGORIE----------------");
		
		// DAOCATEGORIE
		
		Categorie c = new Categorie(10, "Triple");
		
		// Ajout d'une cat�gorie 
		try {
			daocategorie.insert(c);
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
		}
		
		c.setNomCategorie("Quadruple");
		
		// Modification d'une cat�gorie
		try {
			daocategorie.update(c);
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		// Suppression d'une cat�gorie
		try {
			daocategorie.delete(10);
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		// Recherche d'une cat�gorie
		try {
			System.out.println(daocategorie.findById(11).get().getId());
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		// Recherche de toutes les cat�gories
		try {
			System.out.println(daocategorie.findAll().getSize());
		} catch (ExceptionRequest e) {} catch (ExceptionDBAccess e) {
			
		}
		
		System.out.println("");
		
		System.out.println("----------------DAOCHAMBRE----------------");
		
		// DAOCHAMBRE
		
		Categorie ca;
		try {
			// Ajout d'une chambre 
			ca = daocategorie.findById(3).get();
			Chambre ch = new Chambre(2, ca, 3);
			
			daochambre.insert(ch, 3);
			
			// Modification d'une chambre
			ch.getCategorie().setNuCategorie(11);
			daochambre.update(ch, 3);
			System.out.println(ch.getCategorie().getNuCategorie());
			
			// Suppression d'une chambre
			daochambre.delete(ch, 3);
			
			// Recherche d'une chambre
			System.out.println(daochambre.findById(3, 1).getId());
			
			// Recherche de toutes les chambres
			System.out.println(daochambre.findAll().getSize());
		} catch (ExceptionRequest e1) {} catch (ExceptionDBAccess e) {
			
		}

	}

}
